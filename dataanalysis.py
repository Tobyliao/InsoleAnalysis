import csv 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons

import inputdata as data

def average_each_column(indata):
    sum = 0
    # rear
    for i in indata:
        sum+=i
    x = len(indata)
    retavg = sum/x
    return retavg

def Arch_Index(a, b, c):
    return b/(a+b+c)

def Max_Value(value):
    max_num = 0.0
    for i in value:
        if i > max_num:
            max_num = i
    return max_num

def Distributed_Pre(a, b, c):
    return [a/(a+b+c),b/(a+b+c),c/(a+b+c)]

def save_csv(CSV_File,

            Arch_Index_L, Area_Front_L, Area_Mid_L, Area_Rear_L, Area_Total_L,
            Pressure_Front_L, Pressure_Front_Dist_L, Pressure_Front_Max_L,
            Pressure_Mid_L, Pressure_Mid_Dist_L, Pressure_Mid_Max_L, 
            Pressure_Rear_L, Pressure_Rear_Dist_L, Pressure_Rear_Max_L,
            Pressure_Total_L,

            Arch_Index_R, Area_Front_R, Area_Mid_R, Area_Rear_R, Area_Total_R,
            Pressure_Front_R, Pressure_Front_Dist_R, Pressure_Front_Max_R, 
            Pressure_Mid_R, Pressure_Mid_Dist_R, Pressure_Mid_Max_R, 
            Pressure_Rear_R, Pressure_Rear_Dist_R, Pressure_Rear_Max_R,
            Pressure_Total_R,

            back_PAVG_L, mid_PAVG_L, front_PAVG_L,
            back_PAVG_R, mid_PAVG_R, front_PAVG_R,
            back_AAVG_L, mid_AAVG_L, front_AAVG_L,
            back_AAVG_R, mid_AAVG_R, front_AAVG_R):

    with open('./DataBuffer/' + CSV_File, 'w') as csvfile:
        fieldnames = [
        'Arch_Index_L', 'Area_Front_L', 'Area_Mid_L', 'Area_Rear_L', 'Area_Total_L',
        'Pressure_Front_L', 'Pressure_Front_Dist_L', 'Pressure_Front_Max_L', 
        'Pressure_Mid_L', 'Pressure_Mid_Dist_L', 'Pressure_Mid_Max_L', 
        'Pressure_Rear_L', 'Pressure_Rear_Dist_L', 'Pressure_Rear_Max_L',
        'Pressure_Total_L',

        'Arch_Index_R', 'Area_Front_R', 'Area_Mid_R', 'Area_Rear_R', 'Area_Total_R',
        'Pressure_Front_R', 'Pressure_Front_Dist_R', 'Pressure_Front_Max_R', 
        'Pressure_Mid_R', 'Pressure_Mid_Dist_R', 'Pressure_Mid_Max_R', 
        'Pressure_Rear_R', 'Pressure_Rear_Dist_R', 'Pressure_Rear_Max_R',
        'Pressure_Total_R',

        'back_PAVG_L', 'mid_PAVG_L', 'front_PAVG_L',
        'back_PAVG_R', 'mid_PAVG_R', 'front_PAVG_R',
        'back_AAVG_L', 'mid_AAVG_L', 'front_AAVG_L',
        'back_AAVG_R', 'mid_AAVG_R', 'front_AAVG_R']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({

            'Arch_Index_L':Arch_Index_L, 'Area_Front_L':Area_Front_L, 'Area_Mid_L':Area_Mid_L, 'Area_Rear_L':Area_Rear_L, 'Area_Total_L':Area_Total_L,
            'Pressure_Front_L':Pressure_Front_L, 'Pressure_Front_Dist_L':Pressure_Front_Dist_L, 'Pressure_Front_Max_L':Pressure_Front_Max_L, 
            'Pressure_Mid_L':Pressure_Mid_L, 'Pressure_Mid_Dist_L':Pressure_Mid_Dist_L, 'Pressure_Mid_Max_L':Pressure_Mid_Max_L, 
            'Pressure_Rear_L':Pressure_Rear_L, 'Pressure_Rear_Dist_L':Pressure_Rear_Dist_L, 'Pressure_Rear_Max_L':Pressure_Rear_Max_L,
            'Pressure_Total_L':Pressure_Total_L,

            'Arch_Index_R':Arch_Index_R, 'Area_Front_R':Area_Front_R, 'Area_Mid_R':Area_Mid_R, 'Area_Rear_R':Area_Rear_R, 'Area_Total_R':Area_Total_R,
            'Pressure_Front_R':Pressure_Front_R, 'Pressure_Front_Dist_R':Pressure_Front_Dist_R, 'Pressure_Front_Max_R':Pressure_Front_Max_R, 
            'Pressure_Mid_R':Pressure_Mid_R, 'Pressure_Mid_Dist_R':Pressure_Mid_Dist_R, 'Pressure_Mid_Max_R':Pressure_Mid_Max_R, 
            'Pressure_Rear_R':Pressure_Rear_R, 'Pressure_Rear_Dist_R':Pressure_Rear_Dist_R, 'Pressure_Rear_Max_R':Pressure_Rear_Max_R,
            'Pressure_Total_R':Pressure_Total_R,

            'back_PAVG_L':back_PAVG_L, 'mid_PAVG_L':mid_PAVG_L, 'front_PAVG_L':front_PAVG_L,
            'back_PAVG_R':back_PAVG_R, 'mid_PAVG_R':mid_PAVG_R, 'front_PAVG_R':front_PAVG_R,
            'back_AAVG_L':back_AAVG_L, 'mid_AAVG_L':mid_AAVG_L, 'front_AAVG_L':front_AAVG_L,
            'back_AAVG_R':back_AAVG_R, 'mid_AAVG_R':mid_AAVG_R, 'front_AAVG_R':front_AAVG_R})

def save_operate(
    BP_L01,BP_L02,BP_L03,
    MP_L01,MP_L02,MP_L03,
    FP_L01,FP_L02,FP_L03,

    BA_L01,BA_L02,BA_L03,
    MA_L01,MA_L02,MA_L03,
    FA_L01,FA_L02,FA_L03,

    BP_R01,BP_R02,BP_R03,
    MP_R01,MP_R02,MP_R03,
    FP_R01,FP_R02,FP_R03,

    BA_R01,BA_R02,BA_R03,
    MA_R01,MA_R02,MA_R03,
    FA_R01,FA_R02,FA_R03):
    # rear
    avg_pr01_L = average_each_column(BP_L01)
    avg_pr02_L = average_each_column(BP_L02)
    avg_pr03_L = average_each_column(BP_L03)
    # mid
    avg_pm01_L = average_each_column(MP_L01)
    avg_pm02_L = average_each_column(MP_L02)
    avg_pm03_L = average_each_column(MP_L03)
    # fore
    avg_pf01_L = average_each_column(FP_L01)
    avg_pf02_L = average_each_column(FP_L02)
    avg_pf03_L = average_each_column(FP_L03)

    # average_each_column pressure distribution  
    DP01_L = Distributed_Pre(avg_pf01_L, avg_pm01_L, avg_pr01_L)    
    DP02_L = Distributed_Pre(avg_pf02_L, avg_pm02_L, avg_pr02_L)
    DP03_L = Distributed_Pre(avg_pf03_L, avg_pm03_L, avg_pr03_L)

    # rear
    avg_ar01_L = average_each_column(BA_L01)
    avg_ar02_L = average_each_column(BA_L02)
    avg_ar03_L = average_each_column(BA_L03)
    # mid
    avg_am01_L = average_each_column(MA_L01)
    avg_am02_L = average_each_column(MA_L02)
    avg_am03_L = average_each_column(MA_L03)
    # fore
    avg_af01_L = average_each_column(FA_L01)
    avg_af02_L = average_each_column(FA_L02)
    avg_af03_L = average_each_column(FA_L03)

    # Arch Index
    AI01_L = Arch_Index(avg_af01_L, avg_am01_L, avg_ar01_L)
    AI02_L = Arch_Index(avg_af02_L, avg_am02_L, avg_ar02_L)
    AI03_L = Arch_Index(avg_af03_L, avg_am03_L, avg_ar03_L)

    # right
    # rear
    avg_pr01_R = average_each_column(BP_R01)
    avg_pr02_R = average_each_column(BP_R02)
    avg_pr03_R = average_each_column(BP_R03)
    # mid
    avg_pm01_R = average_each_column(MP_R01)
    avg_pm02_R = average_each_column(MP_R02)
    avg_pm03_R = average_each_column(MP_R03)
    # fore
    avg_pf01_R = average_each_column(FP_R01)
    avg_pf02_R = average_each_column(FP_R02)
    avg_pf03_R = average_each_column(FP_R03)

    # average_each_column pressure distribution  
    DP01_R = Distributed_Pre(avg_pf01_R, avg_pm01_R, avg_pr01_R)    
    DP02_R = Distributed_Pre(avg_pf02_R, avg_pm02_R, avg_pr02_R)
    DP03_R = Distributed_Pre(avg_pf03_R, avg_pm03_R, avg_pr03_R)

    # rear
    avg_ar01_R = average_each_column(BA_R01)
    avg_ar02_R = average_each_column(BA_R02)
    avg_ar03_R = average_each_column(BA_R03)
    # mid
    avg_am01_R = average_each_column(MA_R01)
    avg_am02_R = average_each_column(MA_R02)
    avg_am03_R = average_each_column(MA_R03)
    # fore
    avg_af01_R = average_each_column(FA_R01)
    avg_af02_R = average_each_column(FA_R02)
    avg_af03_R = average_each_column(FA_R03)

    # Arch Index
    AI01_R = Arch_Index(avg_af01_R, avg_am01_R, avg_ar01_R)
    AI02_R = Arch_Index(avg_af02_R, avg_am02_R, avg_ar02_R)
    AI03_R = Arch_Index(avg_af03_R, avg_am03_R, avg_ar03_R)

    back_PAVG_L = []
    mid_PAVG_L = []
    front_PAVG_L = []

    back_PAVG_R = []
    mid_PAVG_R = []
    front_PAVG_R = []

    back_AAVG_L = []
    mid_AAVG_L = []
    front_AAVG_L = []

    back_AAVG_R = []
    mid_AAVG_R = []
    front_AAVG_R = []

    FP01_MAX_L = Max_Value(FP_L01)
    FP02_MAX_L = Max_Value(FP_L02)
    FP03_MAX_L = Max_Value(FP_L03)

    MP01_MAX_L = Max_Value(MP_L01)
    MP02_MAX_L = Max_Value(MP_L02)
    MP03_MAX_L = Max_Value(MP_L03)

    RP01_MAX_L = Max_Value(BP_L01)
    RP02_MAX_L = Max_Value(BP_L02)
    RP03_MAX_L = Max_Value(BP_L03)

    FP01_MAX_R = Max_Value(FP_R01)
    FP02_MAX_R = Max_Value(FP_R02)
    FP03_MAX_R = Max_Value(FP_R03)

    MP01_MAX_R = Max_Value(MP_R01)
    MP02_MAX_R = Max_Value(MP_R02)
    MP03_MAX_R = Max_Value(MP_R03)

    RP01_MAX_R = Max_Value(BP_R01)
    RP02_MAX_R = Max_Value(BP_R02)
    RP03_MAX_R = Max_Value(BP_R03)

    if labcla == 'blue':
        AI_L = (AI02_L+AI03_L)/2
        DP_L = [(DP02_L[0]+DP03_L[0])/2,(DP02_L[1]+DP03_L[1])/2,(DP02_L[2]+DP03_L[2])/2]
        AI_R = (AI02_R+AI03_R)/2
        DP_R = [(DP02_R[0]+DP03_R[0])/2,(DP02_R[1]+DP03_R[1])/2,(DP02_R[2]+DP03_R[2])/2]
        
        Area_Front_L = (avg_af02_L + avg_af03_L) / 2
        Area_Mid_L =  (avg_am02_L + avg_am03_L) / 2
        Area_Rear_L = (avg_ar02_L + avg_ar03_L) / 2
        Area_Total_L = (Area_Front_L + Area_Mid_L + Area_Rear_L)


        Area_Front_R = (avg_af02_R + avg_af03_R) / 2
        Area_Mid_R =  (avg_am02_R + avg_am03_R) / 2
        Area_Rear_R = (avg_ar02_R + avg_ar03_R) / 2
        Area_Total_R = (Area_Front_R + Area_Mid_R + Area_Rear_R)

        Pressure_Front_L = (avg_pf02_L + avg_pf03_L) / 2
        Pressure_Mid_L =  (avg_pm02_L + avg_pm03_L) / 2
        Pressure_Rear_L = (avg_pr02_L + avg_pr03_L) / 2
        Pressure_Total_L = (Pressure_Front_L + Pressure_Mid_L + Pressure_Rear_L) / 3

        Pressure_Front_R = (avg_pf02_R + avg_pf03_R) / 2
        Pressure_Mid_R =  (avg_pm02_R + avg_pm03_R) / 2
        Pressure_Rear_R = (avg_pr02_R + avg_pr03_R) / 2
        Pressure_Total_R = (Pressure_Front_R + Pressure_Mid_R + Pressure_Rear_R) / 3

        Pressure_Front_Max_L = max(FP02_MAX_L,FP03_MAX_L) 
        Pressure_Mid_Max_L = max(MP02_MAX_L,MP03_MAX_L) 
        Pressure_Rear_Max_L = max(RP02_MAX_L,RP03_MAX_L) 

        Pressure_Front_Max_R = max(FP02_MAX_R,FP03_MAX_R) 
        Pressure_Mid_Max_R = max(MP02_MAX_R,MP03_MAX_R) 
        Pressure_Rear_Max_R = max(RP02_MAX_R,RP03_MAX_R)

        
        for i in range(0,len(data.back_PS01_L01)):
            back_PAVG_L.append((BP_L02[i]+BP_L03[i])/2)
            mid_PAVG_L.append((MP_L02[i]+MP_L03[i])/2)
            front_PAVG_L.append((FP_L02[i]+FP_L03[i])/2)

            back_PAVG_R.append((BP_R02[i]+BP_R03[i])/2)
            mid_PAVG_R.append((MP_R02[i]+MP_R03[i])/2)
            front_PAVG_R.append((FP_R02[i]+FP_R03[i])/2)

            back_AAVG_L.append((BA_L02[i]+BA_L03[i])/2)
            mid_AAVG_L.append((MA_L02[i]+MA_L03[i])/2)
            front_AAVG_L.append((FA_L02[i]+FA_L03[i])/2)

            back_AAVG_R.append((BA_R02[i]+BA_R03[i])/2)
            mid_AAVG_R.append((MA_R02[i]+MA_R03[i])/2)
            front_AAVG_R.append((FA_R02[i]+FA_R03[i])/2)

    elif labcla == 'red':
        AI_L = (AI01_L+AI03_L)/2
        DP_L = [(DP01_L[0]+DP03_L[0])/2,(DP01_L[1]+DP03_L[1])/2,(DP01_L[2]+DP03_L[2])/2]
        AI_R = (AI01_R+AI03_R)/2
        DP_R = [(DP01_R[0]+DP03_R[0])/2,(DP01_R[1]+DP03_R[1])/2,(DP01_R[2]+DP03_R[2])/2]

        Area_Front_L = (avg_af01_L + avg_af03_L) / 2
        Area_Mid_L =  (avg_am01_L + avg_am03_L) / 2
        Area_Rear_L = (avg_ar01_L + avg_ar03_L) / 2
        Area_Total_L = (Area_Front_L + Area_Mid_L + Area_Rear_L) 


        Area_Front_R = (avg_af01_R + avg_af03_R) / 2
        Area_Mid_R =  (avg_am01_R + avg_am03_R) / 2
        Area_Rear_R = (avg_ar01_R + avg_ar03_R) / 2
        Area_Total_R = (Area_Front_R + Area_Mid_R + Area_Rear_R) 

        Pressure_Front_L = (avg_pf01_L + avg_pf03_L) / 2
        Pressure_Mid_L =  (avg_pm01_L + avg_pm03_L) / 2
        Pressure_Rear_L = (avg_pr01_L + avg_pr03_L) / 2
        Pressure_Total_L = (Pressure_Front_L + Pressure_Mid_L + Pressure_Rear_L) / 3

        Pressure_Front_R = (avg_pf01_R + avg_pf03_R) / 2
        Pressure_Mid_R =  (avg_pm01_R + avg_pm03_R) / 2
        Pressure_Rear_R = (avg_pr01_R + avg_pr03_R) / 2
        Pressure_Total_R = (Pressure_Front_R + Pressure_Mid_R + Pressure_Rear_R) / 3

        Pressure_Front_Max_L = max(FP01_MAX_L,FP03_MAX_L) 
        Pressure_Mid_Max_L = max(MP01_MAX_L,MP03_MAX_L) 
        Pressure_Rear_Max_L = max(RP01_MAX_L,RP03_MAX_L) 

        Pressure_Front_Max_R = max(FP01_MAX_R,FP03_MAX_R) 
        Pressure_Mid_Max_R = max(MP01_MAX_R,MP03_MAX_R) 
        Pressure_Rear_Max_R = max(RP01_MAX_R,RP03_MAX_R) 

        for i in range(0,len(data.back_PS01_L01)):
            back_PAVG_L.append((BP_L01[i] + BP_L03[i]) / 2)
            mid_PAVG_L.append((MP_L01[i] + MP_L03[i]) / 2)
            front_PAVG_L.append((FP_L01[i] + FP_L03[i]) / 2)

            back_PAVG_R.append((BP_R01[i] + BP_R03[i]) / 2)
            mid_PAVG_R.append((MP_R01[i] + MP_R03[i]) / 2)
            front_PAVG_R.append((FP_R01[i] + FP_R03[i]) / 2)

            back_AAVG_L.append((BA_L01[i] + BA_L03[i]) / 2)
            mid_AAVG_L.append((MA_L01[i] + MA_L03[i]) / 2)
            front_AAVG_L.append((FA_L01[i] + FA_L03[i]) / 2)

            back_AAVG_R.append((BA_R01[i] + BA_R03[i]) / 2)
            mid_AAVG_R.append((MA_R01[i] + MA_R03[i]) / 2)
            front_AAVG_R.append((FA_R01[i] + FA_R03[i]) / 2)

    elif labcla == 'green':
        AI_L = (AI01_L+AI02_L)/2
        DP_L = [(DP01_L[0]+DP02_L[0])/2,(DP01_L[1]+DP02_L[1])/2,(DP01_L[2]+DP02_L[2])/2]
        AI_R = (AI01_R+AI02_R)/2
        DP_R = [(DP01_R[0]+DP02_R[0])/2,(DP01_R[1]+DP02_R[1])/2,(DP01_R[2]+DP02_R[2])/2]

        Area_Front_L = (avg_af01_L + avg_af02_L) / 2
        Area_Mid_L =  (avg_am01_L + avg_am02_L) / 2
        Area_Rear_L = (avg_ar01_L + avg_ar02_L) / 2
        Area_Total_L = (Area_Front_L + Area_Mid_L + Area_Rear_L) 


        Area_Front_R = (avg_af01_R + avg_af02_R) / 2
        Area_Mid_R =  (avg_am01_R + avg_am02_R) / 2
        Area_Rear_R = (avg_ar01_R + avg_ar02_R) / 2
        Area_Total_R = (Area_Front_R + Area_Mid_R + Area_Rear_R) 

        Pressure_Front_L = (avg_pf01_L + avg_pf02_L) / 2
        Pressure_Mid_L =  (avg_pm01_L + avg_pm02_L) / 2
        Pressure_Rear_L = (avg_pr01_L + avg_pr02_L) / 2
        Pressure_Total_L = (Pressure_Front_L + Pressure_Mid_L + Pressure_Rear_L) / 3

        Pressure_Front_R = (avg_pf01_R + avg_pf02_R) / 2
        Pressure_Mid_R =  (avg_pm01_R + avg_pm02_R) / 2
        Pressure_Rear_R = (avg_pr01_R + avg_pr02_R) / 2
        Pressure_Total_R = (Pressure_Front_R + Pressure_Mid_R + Pressure_Rear_R) / 3

        Pressure_Front_Max_L = max(FP01_MAX_L,FP02_MAX_L) 
        Pressure_Mid_Max_L = max(MP01_MAX_L,MP02_MAX_L) 
        Pressure_Rear_Max_L = max(RP01_MAX_L,RP02_MAX_L) 

        Pressure_Front_Max_R = max(FP01_MAX_R,FP02_MAX_R) 
        Pressure_Mid_Max_R = max(MP01_MAX_R,MP02_MAX_R) 
        Pressure_Rear_Max_R = max(RP01_MAX_R,RP02_MAX_R) 
 

        for i in range(0,len(data.back_PS01_L01)):
            back_PAVG_L.append((BP_L01[i]+BP_L02[i])/2)
            mid_PAVG_L.append((MP_L01[i]+MP_L02[i])/2)
            front_PAVG_L.append((FP_L01[i]+FP_L02[i])/2)

            back_PAVG_R.append((BP_R01[i]+BP_R02[i])/2)
            mid_PAVG_R.append((MP_R01[i]+MP_R02[i])/2)
            front_PAVG_R.append((FP_R01[i]+FP_R02[i])/2)

            back_AAVG_L.append((BA_L01[i]+BA_L02[i])/2)
            mid_AAVG_L.append((MA_L01[i]+MA_L02[i])/2)
            front_AAVG_L.append((FA_L01[i]+FA_L02[i])/2)

            back_AAVG_R.append((BA_R01[i]+BA_R02[i])/2)
            mid_AAVG_R.append((MA_R01[i]+MA_R02[i])/2)
            front_AAVG_R.append((FA_R01[i]+FA_R02[i])/2)

    else:
        AI_L = (AI01_L+AI02_L+AI03_L)/3
        DP_L = [(DP01_L[0]+DP02_L[0]+DP03_L[0])/3,
        (DP01_L[1]+DP02_L[1]+DP03_L[1])/3,(DP01_L[2]+DP02_L[2]+DP03_L[2])/3]
        AI_R = (AI01_R+AI02_R+AI03_R)/3
        DP_R = [(DP01_R[0]+DP02_R[0]+DP03_R[0])/3,
        (DP01_R[1]+DP02_R[1]+DP03_R[1])/3,(DP01_R[2]+DP02_R[2]+DP03_R[2])/3]

        Area_Front_L = (avg_af01_L + avg_af02_L + avg_af03_L) / 3
        Area_Mid_L =  (avg_am01_L + avg_am02_L + avg_am03_L) / 3
        Area_Rear_L = (avg_ar01_L + avg_ar02_L + avg_ar03_L) / 3
        Area_Total_L = (Area_Front_L + Area_Mid_L + Area_Rear_L)


        Area_Front_R = (avg_af01_R + avg_af02_R + avg_af03_R) / 3
        Area_Mid_R =  (avg_am01_R + avg_am02_R + avg_am03_R) / 3
        Area_Rear_R = (avg_ar01_R + avg_ar02_R + avg_ar03_R) / 3
        Area_Total_R = (Area_Front_R + Area_Mid_R + Area_Rear_R)

        Pressure_Front_L = (avg_pf01_L + avg_pf02_L + avg_pf03_L) / 3
        Pressure_Mid_L =  (avg_pm01_L + avg_pm02_L + avg_pm03_L) / 3
        Pressure_Rear_L = (avg_pr01_L + avg_pr02_L + avg_pr03_L) / 3
        Pressure_Total_L = (Pressure_Front_L + Pressure_Mid_L + Pressure_Rear_L) / 3


        Pressure_Front_R = (avg_pf01_R + avg_pf02_R + avg_pf03_R) / 3
        Pressure_Mid_R =  (avg_pm01_R + avg_pm02_R + avg_pm03_R) / 3
        Pressure_Rear_R = (avg_pr01_R + avg_pr02_R + avg_pr03_R) / 3
        Pressure_Total_R = (Pressure_Front_R + Pressure_Mid_R + Pressure_Rear_R) / 3

        Pressure_Front_Max_L = max(FP01_MAX_L,FP02_MAX_L,FP03_MAX_L) 
        Pressure_Mid_Max_L = max(MP01_MAX_L,MP02_MAX_L,MP03_MAX_L) 
        Pressure_Rear_Max_L = max(RP01_MAX_L,RP02_MAX_L,RP02_MAX_L) 

        Pressure_Front_Max_R = max(FP01_MAX_R,FP02_MAX_R,FP03_MAX_R) 
        Pressure_Mid_Max_R = max(MP01_MAX_R,MP02_MAX_R,MP03_MAX_R) 
        Pressure_Rear_Max_R = max(RP01_MAX_R,RP02_MAX_R,RP03_MAX_R) 

        for i in range(0,len(data.back_PS01_L01)):
            back_PAVG_L.append((BP_L01[i]+BP_L02[i]+BP_L03[i])/3)
            mid_PAVG_L.append((MP_L01[i]+MP_L02[i]+MP_L03[i])/3) 
            front_PAVG_L.append((FP_L01[i]+FP_L02[i]+FP_L03[i])/3) 

            back_PAVG_R.append((BP_R01[i]+BP_R02[i]+BP_R03[i])/3) 
            mid_PAVG_R.append((MP_R01[i]+MP_R02[i]+MP_R03[i])/3) 
            front_PAVG_R.append((FP_R01[i]+FP_R02[i]+FP_R03[i])/3) 

            back_AAVG_L.append((BA_L01[i]+BA_L02[i]+BA_L03[i])/3) 
            mid_AAVG_L.append((MA_L01[i]+MA_L02[i]+MA_L03[i])/3) 
            front_AAVG_L.append((FA_L01[i]+FA_L02[i]+FA_L03[i])/3) 

            back_AAVG_R.append((BA_R01[i]+BA_R02[i]+BA_R03[i])/3) 
            mid_AAVG_R.append((MA_R01[i]+MA_R02[i]+MA_R03[i])/3) 
            front_AAVG_R.append((FA_R01[i]+FA_R02[i]+FA_R03[i])/3) 

    save_csv('Normal_S0%s_BStanding.csv'%(number),
            AI_L, Area_Front_L, Area_Mid_L, Area_Rear_L, Area_Total_L,
            Pressure_Front_L, DP_L[2], Pressure_Front_Max_L, 
            Pressure_Mid_L, DP_L[1], Pressure_Mid_Max_L, 
            Pressure_Rear_L, DP_L[0], Pressure_Rear_Max_L,
            Pressure_Total_L,

            AI_R, Area_Front_R, Area_Mid_R, Area_Rear_R, Area_Total_R,
            Pressure_Front_R, DP_R[2], Pressure_Front_Max_R, 
            Pressure_Mid_R, DP_R[1], Pressure_Mid_Max_R, 
            Pressure_Rear_R, DP_R[0], Pressure_Rear_Max_R,
            Pressure_Total_R,

            back_PAVG_L, mid_PAVG_L, front_PAVG_L,
            back_PAVG_R, mid_PAVG_R, front_PAVG_R,
            back_AAVG_L, mid_AAVG_L, front_AAVG_L,
            back_AAVG_R, mid_AAVG_R, front_AAVG_R)
    # labcla = 'all'

def save(event):
    # print(display.label)
    # Write CSV file
    # 1.Arch Index:b/(a+b+c)(b mid area and abc stand for average area)
    # 2.average pressure distribution:fore a/(a+b+c)、mid b/(a+b+c)、rear c/(a+b+c)(abc stand for average pressure)
    if number == 1 :
        save_operate(
        data.back_PS01_L01, data.back_PS01_L02, data.back_PS01_L03,
        data.mid_PS01_L01,  data.mid_PS01_L02,  data.mid_PS01_L03,
        data.front_PS01_L01,data.front_PS01_L02,data.front_PS01_L03,

        data.back_AS01_L01, data.back_AS01_L02, data.back_AS01_L03,
        data.mid_AS01_L01,  data.mid_AS01_L02,  data.mid_AS01_L03,
        data.front_AS01_L01,data.front_AS01_L02,data.front_AS01_L03,

        data.back_PS01_R01, data.back_PS01_R02, data.back_PS01_R03,
        data.mid_PS01_R01,  data.mid_PS01_R02,  data.mid_PS01_R03,
        data.front_PS01_R01,data.front_PS01_R02,data.front_PS01_R03,

        data.back_AS01_R01, data.back_AS01_R02, data.back_AS01_R03,
        data.mid_AS01_R01,  data.mid_AS01_R02,  data.mid_AS01_R03,
        data.front_AS01_R01,data.front_AS01_R02,data.front_AS01_R03)
   
    elif number == 2 :
        save_operate(
        data.back_PS02_L01, data.back_PS02_L02, data.back_PS02_L03,
        data.mid_PS02_L01,  data.mid_PS02_L02,  data.mid_PS02_L03,
        data.front_PS02_L01,data.front_PS02_L02,data.front_PS02_L03,

        data.back_AS02_L01, data.back_AS02_L02, data.back_AS02_L03,
        data.mid_AS02_L01,  data.mid_AS02_L02,  data.mid_AS02_L03,
        data.front_AS02_L01,data.front_AS02_L02,data.front_AS02_L03,

        data.back_PS02_R01, data.back_PS02_R02, data.back_PS02_R03,
        data.mid_PS02_R01,  data.mid_PS02_R02,  data.mid_PS02_R03,
        data.front_PS02_R01,data.front_PS02_R02,data.front_PS02_R03,

        data.back_AS02_R01, data.back_AS02_R02, data.back_AS02_R03,
        data.mid_AS02_R01,  data.mid_AS02_R02,  data.mid_AS02_R03,
        data.front_AS02_R01,data.front_AS02_R02,data.front_AS02_R03)

    elif number == 3 :
        save_operate(
        data.back_PS03_L01, data.back_PS03_L02, data.back_PS03_L03,
        data.mid_PS03_L01,  data.mid_PS03_L02,  data.mid_PS03_L03,
        data.front_PS03_L01,data.front_PS03_L02,data.front_PS03_L03,

        data.back_AS03_L01, data.back_AS03_L02, data.back_AS03_L03,
        data.mid_AS03_L01,  data.mid_AS03_L02,  data.mid_AS03_L03,
        data.front_AS03_L01,data.front_AS03_L02,data.front_AS03_L03,

        data.back_PS03_R01, data.back_PS03_R02, data.back_PS03_R03,
        data.mid_PS03_R01,  data.mid_PS03_R02,  data.mid_PS03_R03,
        data.front_PS03_R01,data.front_PS03_R02,data.front_PS03_R03,

        data.back_AS03_R01, data.back_AS03_R02, data.back_AS03_R03,
        data.mid_AS03_R01,  data.mid_AS03_R02,  data.mid_AS03_R03,
        data.front_AS03_R01,data.front_AS03_R02,data.front_AS03_R03)
        
    done_one_time = 1
    print("done_one_time_save =",done_one_time)
    print("num_save =",number)
    global_Increment()
    if number>int(num):
        plt.clf()
        plt.suptitle("Average Values of all Patients",x=0.5)
        print("Exit")

def plot(label):
    display.genBtn()
    plot_diagram(label)

def plot_feature(label,feanum,feaname,file1,file2,file3):
    # plot the heel graph
    plt.subplot(4,4,feanum)
    plt.xlim([0,100])
    plt.ylim([0,100])
    plt.title(feaname)
    t = np.arange(0, 100, 1)
    if feanum >12:
        plt.tick_params(labeltop='off', labelright='off')
    else:
        plt.tick_params(labeltop='off', labelright='off', labelbottom='off')
    if label == "red":
        plt.plot(t, file2, 'g')
        plt.plot(t, file3, 'b')
    elif label == "blue":
        plt.plot(t, file1, 'r')
        plt.plot(t, file2, 'g')
    elif label == "green":
        plt.plot(t, file1, 'r')
        plt.plot(t, file3, 'b')
    elif label == "all":
        plt.plot(t, file1, 'r')
        plt.plot(t, file2, 'g')
        plt.plot(t, file3, 'b')
    global labcla
    labcla = label

def plot_diagram_final():
    # plot the heel graph
    plot_feature(label,1,'Pressure_Rear_Left',
        data.back_PS01_L01,data.back_PS01_L02,data.back_PS01_L03)
    # plot the Mid graph
    plot_feature(label,2,'Pressure_Mid_Left',
        data.mid_PS01_L01,data.mid_PS01_L02,data.mid_PS01_L03)
    # plot the front graph
    plot_feature(label,3,'Pressure_Fore_Left',
        data.front_PS01_L01,data.front_PS01_L02,data.front_PS01_L03)
    # plot the all graph
    plot_feature(label,4,'Pressure_All_Left',
        data.all_PS01_L01,data.all_PS01_L02,data.all_PS01_L03)
    #Area Data
    # plot the heel graph
    plot_feature(label,5,'Area_Rear_Left',
        data.back_AS01_L01,data.back_AS01_L02,data.back_AS01_L03)
    # plot the Mid graph
    plot_feature(label,6,'Area_Mid_Left',
        data.mid_AS01_L01,data.mid_AS01_L02,data.mid_AS01_L03)
    # plot the front graph
    plot_feature(label,7,'Area_Fore_Left',
        data.front_AS01_L01,data.front_AS01_L02,data.front_AS01_L03)
    # plot the all graph
    plot_feature(label,8,'Area_All_Left',
        data.all_AS01_L01,data.all_AS01_L02,data.all_AS01_L03)
    # plot the heel graph
    plot_feature(label,9,'Pressure_Rear_Right',
        data.back_PS01_R01,data.back_PS01_R02,data.back_PS01_R03)
    # plot the Mid graph
    plot_feature(label,10,'Pressure_Mid_Right',
        data.mid_PS01_R01,data.mid_PS01_R02,data.mid_PS01_R03)
    # plot the front graph
    plot_feature(label,11,'Pressure_Fore_Right',
        data.front_PS01_R01,data.front_PS01_R02,data.front_PS01_R03)
    # plot the all graph
    plot_feature(label,12,'Pressure_All_Right',
        data.all_PS01_R01,data.all_PS01_R02,data.all_PS01_R03)
    #Area Data
    # plot the heel graph
    plot_feature(label,13,'Area_Rear_Right',
        data.back_AS01_R01,data.back_AS01_R02,data.back_AS01_R03)
    # plot the Mid graph
    plot_feature(label,14,'Area_Mid_Right',
        data.mid_AS01_R01,data.mid_AS01_R02,data.mid_AS01_R03)
    # plot the front graph
    plot_feature(label,15,'Area_Fore_Right',
        data.front_AS01_R01,data.front_AS01_R02,data.front_AS01_R03)
    # plot the all graph
    plot_feature(label,16,'Area_All_Right',
        data.all_AS01_R01,data.all_AS01_R02,data.all_AS01_R03)

def plot_diagram(label):
    if number == 1:
        # plot the heel graph
        plot_feature(label,1,'Pressure_Rear_Left',
            data.back_PS01_L01,data.back_PS01_L02,data.back_PS01_L03)
        # plot the Mid graph
        plot_feature(label,2,'Pressure_Mid_Left',
            data.mid_PS01_L01,data.mid_PS01_L02,data.mid_PS01_L03)
        # plot the front graph
        plot_feature(label,3,'Pressure_Fore_Left',
            data.front_PS01_L01,data.front_PS01_L02,data.front_PS01_L03)
        # plot the all graph
        plot_feature(label,4,'Pressure_All_Left',
            data.all_PS01_L01,data.all_PS01_L02,data.all_PS01_L03)
        #Area Data
        # plot the heel graph
        plot_feature(label,5,'Area_Rear_Left',
            data.back_AS01_L01,data.back_AS01_L02,data.back_AS01_L03)
        # plot the Mid graph
        plot_feature(label,6,'Area_Mid_Left',
            data.mid_AS01_L01,data.mid_AS01_L02,data.mid_AS01_L03)
        # plot the front graph
        plot_feature(label,7,'Area_Fore_Left',
            data.front_AS01_L01,data.front_AS01_L02,data.front_AS01_L03)
        # plot the all graph
        plot_feature(label,8,'Area_All_Left',
            data.all_AS01_L01,data.all_AS01_L02,data.all_AS01_L03)
        # plot the heel graph
        plot_feature(label,9,'Pressure_Rear_Right',
            data.back_PS01_R01,data.back_PS01_R02,data.back_PS01_R03)
        # plot the Mid graph
        plot_feature(label,10,'Pressure_Mid_Right',
            data.mid_PS01_R01,data.mid_PS01_R02,data.mid_PS01_R03)
        # plot the front graph
        plot_feature(label,11,'Pressure_Fore_Right',
            data.front_PS01_R01,data.front_PS01_R02,data.front_PS01_R03)
        # plot the all graph
        plot_feature(label,12,'Pressure_All_Right',
            data.all_PS01_R01,data.all_PS01_R02,data.all_PS01_R03)
        #Area Data
        # plot the heel graph
        plot_feature(label,13,'Area_Rear_Right',
            data.back_AS01_R01,data.back_AS01_R02,data.back_AS01_R03)
        # plot the Mid graph
        plot_feature(label,14,'Area_Mid_Right',
            data.mid_AS01_R01,data.mid_AS01_R02,data.mid_AS01_R03)
        # plot the front graph
        plot_feature(label,15,'Area_Fore_Right',
            data.front_AS01_R01,data.front_AS01_R02,data.front_AS01_R03)
        # plot the all graph
        plot_feature(label,16,'Area_All_Right',
            data.all_AS01_R01,data.all_AS01_R02,data.all_AS01_R03)

    elif number == 2:
        # plot the heel graph
        plot_feature(label,1,'Pressure_Rear_Left',
            data.back_PS02_L01,data.back_PS02_L02,data.back_PS02_L03)
        # plot the Mid graph
        plot_feature(label,2,'Pressure_Mid_Left',
            data.mid_PS02_L01,data.mid_PS02_L02,data.mid_PS02_L03)
        # plot the front graph
        plot_feature(label,3,'Pressure_Fore_Left',
            data.front_PS02_L01,data.front_PS02_L02,data.front_PS02_L03)
        # plot the all graph
        plot_feature(label,4,'Pressure_All_Left',
            data.all_PS02_L01,data.all_PS02_L02,data.all_PS02_L03)
        #Area Data
        # plot the heel graph
        plot_feature(label,5,'Area_Rear_Left',
            data.back_AS02_L01,data.back_AS02_L02,data.back_AS02_L03)
        # plot the Mid graph
        plot_feature(label,6,'Area_Mid_Left',
            data.mid_AS02_L01,data.mid_AS02_L02,data.mid_AS02_L03)
        # plot the front graph
        plot_feature(label,7,'Area_Fore_Left',
            data.front_AS02_L01,data.front_AS02_L02,data.front_AS02_L03)
        # plot the all graph
        plot_feature(label,8,'Area_All_Left',
            data.all_AS02_L01,data.all_AS02_L02,data.all_AS02_L03)
        # plot the heel graph
        plot_feature(label,9,'Pressure_Rear_Right',
            data.back_PS02_R01,data.back_PS02_R02,data.back_PS02_R03)
        # plot the Mid graph
        plot_feature(label,10,'Pressure_Mid_Right',
            data.mid_PS02_R01,data.mid_PS02_R02,data.mid_PS02_R03)
        # plot the front graph
        plot_feature(label,11,'Pressure_Fore_Right',
            data.front_PS02_R01,data.front_PS02_R02,data.front_PS02_R03)
        # plot the all graph
        plot_feature(label,12,'Pressure_All_Right',
            data.all_PS02_R01,data.all_PS02_R02,data.all_PS02_R03)
        #Area Data
        # plot the heel graph
        plot_feature(label,13,'Area_Rear_Right',
            data.back_AS02_R01,data.back_AS02_R02,data.back_AS02_R03)
        # plot the Mid graph
        plot_feature(label,14,'Area_Mid_Right',
            data.mid_AS02_R01,data.mid_AS02_R02,data.mid_AS02_R03)
        # plot the front graph
        plot_feature(label,15,'Area_Fore_Right',
            data.front_AS02_R01,data.front_AS02_R02,data.front_AS02_R03)
        # plot the all graph
        plot_feature(label,16,'Area_All_Right',
            data.all_AS02_R01,data.all_AS02_R02,data.all_AS02_R03)
        
    elif number == 3:
        # plot the heel graph
        plot_feature(label,1,'Pressure_Rear_Left',
            data.back_PS03_L01,data.back_PS03_L02,data.back_PS03_L03)
        # plot the Mid graph
        plot_feature(label,2,'Pressure_Mid_Left',
            data.mid_PS03_L01,data.mid_PS03_L02,data.mid_PS03_L03)
        # plot the front graph
        plot_feature(label,3,'Pressure_Fore_Left',
            data.front_PS03_L01,data.front_PS03_L02,data.front_PS03_L03)
        # plot the all graph
        plot_feature(label,4,'Pressure_All_Left',
            data.all_PS03_L01,data.all_PS03_L02,data.all_PS03_L03)
        #Area Data
        # plot the heel graph
        plot_feature(label,5,'Area_Rear_Left',
            data.back_AS03_L01,data.back_AS03_L02,data.back_AS03_L03)
        # plot the Mid graph
        plot_feature(label,6,'Area_Mid_Left',
            data.mid_AS03_L01,data.mid_AS03_L02,data.mid_AS03_L03)
        # plot the front graph
        plot_feature(label,7,'Area_Fore_Left',
            data.front_AS03_L01,data.front_AS03_L02,data.front_AS03_L03)
        # plot the all graph
        plot_feature(label,8,'Area_All_Left',
            data.all_AS03_L01,data.all_AS03_L02,data.all_AS03_L03)
        # plot the heel graph
        plot_feature(label,9,'Pressure_Rear_Right',
            data.back_PS03_R01,data.back_PS03_R02,data.back_PS03_R03)
        # plot the Mid graph
        plot_feature(label,10,'Pressure_Mid_Right',
            data.mid_PS03_R01,data.mid_PS03_R02,data.mid_PS03_R03)
        # plot the front graph
        plot_feature(label,11,'Pressure_Fore_Right',
            data.front_PS03_R01,data.front_PS03_R02,data.front_PS03_R03)
        # plot the all graph
        plot_feature(label,12,'Pressure_All_Right',
            data.all_PS03_R01,data.all_PS03_R02,data.all_PS03_R03)
        #Area Data
        # plot the heel graph
        plot_feature(label,13,'Area_Rear_Right',
            data.back_AS03_R01,data.back_AS03_R02,data.back_AS03_R03)
        # plot the Mid graph
        plot_feature(label,14,'Area_Mid_Right',
            data.mid_AS03_R01,data.mid_AS03_R02,data.mid_AS03_R03)
        # plot the front graph
        plot_feature(label,15,'Area_Fore_Right',
            data.front_AS03_R01,data.front_AS03_R02,data.front_AS03_R03)
        # plot the all graph
        plot_feature(label,16,'Area_All_Right',
            data.all_AS03_R01,data.all_AS03_R02,data.all_AS03_R03)

class Buttons:
    def __init__(self): #initial the Data
        self.axcolor= 'lightgoldenrodyellow'
        self.rax = plt.axes([0.01, 0.3, 0.06, 0.301], axisbg=self.axcolor)
        self.radio = RadioButtons(self.rax, ( 'blue', 'red', 'green', 'all'), active=0)
        self.del_ax = plt.axes([0.01, 0.25, 0.085, 0.035])
        self.button_save = Button(self.del_ax, 'Save', color=self.axcolor, hovercolor='0.975')
        self.label = "all"
        self.radio.on_clicked(plot)
        self.button_save.on_clicked(save)

    def genBtn(self):
        plt.clf()
        plt.suptitle("Normal_S0%s_BStanding"%(number),x=0.5)
        self.__init__()

def global_Increment():
	global number
	number = int(number) +1

labcla = ""
done_one_time = 0
display = Buttons()
global num
num = input("Patient's number = ")	
number = 1
plt.suptitle("Normal_S0%s_BStanding"%(number),x=0.5)
plot_diagram(display.label)
plt.show()




