import numpy as np

def linsp(value,interval_num):
    length = len(value)
    rtnvalue = []
    routnum = length // interval_num +1
    inter = routnum
    avg = 0
    sumval = 0 
    for num in value:
        inter = inter - 1 
        sumval = sumval + num
        if inter == 0:
            avg = sumval / routnum
            rtnvalue.append(avg)
            inter = routnum
            sumval = 0
    return rtnvalue

# S01
# back_PS01_L01
# Normal_PS01_L01(Python3)
Normal_PS01_L01 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_L01 = linsp(Normal_PS01_L01[1:601,4],101)
mid_PS01_L01 = linsp(Normal_PS01_L01[1:601,5],101)
front_PS01_L01 = linsp(Normal_PS01_L01[1:601,6],101)
all_PS01_L01 = linsp(Normal_PS01_L01[1:601,7],101)

# # Normal_PS01_L02(Python3)
Normal_PS01_L02 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_L02 = linsp(Normal_PS01_L02[1:601,4],101)
mid_PS01_L02 = linsp(Normal_PS01_L02[1:601,5],101)
front_PS01_L02 = linsp(Normal_PS01_L02[1:601,6],101)
all_PS01_L02 = linsp(Normal_PS01_L02[1:601,7],101)

# # Normal_PS01_L01(Python3)
Normal_PS01_L03 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_L03 = linsp(Normal_PS01_L03[1:601,4],101)
mid_PS01_L03 = linsp(Normal_PS01_L03[1:601,5],101)
front_PS01_L03 = linsp(Normal_PS01_L03[1:601,6],101)
all_PS01_L03 = linsp(Normal_PS01_L03[1:601,7],101)

# # Normal_PS01_R01(Python3)
Normal_PS01_R01 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_R01 = linsp(Normal_PS01_R01[1:601,4],101)
mid_PS01_R01 = linsp(Normal_PS01_R01[1:601,5],101)
front_PS01_R01 = linsp(Normal_PS01_R01[1:601,6],101)
all_PS01_R01 = linsp(Normal_PS01_R01[1:601,7],101)

# # Normal_PS01_R02(Python3)
Normal_PS01_R02 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_R02 = linsp(Normal_PS01_R02[1:601,4],101)
mid_PS01_R02 = linsp(Normal_PS01_R02[1:601,5],101)
front_PS01_R02 = linsp(Normal_PS01_R02[1:601,6],101)
all_PS01_R02 = linsp(Normal_PS01_R02[1:601,7],101)

# # Normal_PS01_R01(Python3)
Normal_PS01_R03 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Pressure_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS01_R03 = linsp(Normal_PS01_R03[1:601,4],101)
mid_PS01_R03 = linsp(Normal_PS01_R03[1:601,5],101)
front_PS01_R03 = linsp(Normal_PS01_R03[1:601,6],101)
all_PS01_R03 = linsp(Normal_PS01_R03[1:601,7],101)

# back_AS01_L01
# Normal_AS01_L01(Python3)
Normal_AS01_L01 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_L01 = linsp(Normal_AS01_L01[1:601,4],101)
mid_AS01_L01 = linsp(Normal_AS01_L01[1:601,5],101)
front_AS01_L01 = linsp(Normal_AS01_L01[1:601,6],101)
all_AS01_L01 = linsp(Normal_AS01_L01[1:601,7],101)

# # Normal_AS01_L02(Python3)
Normal_AS01_L02 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_L02 = linsp(Normal_AS01_L02[1:601,4],101)
mid_AS01_L02 = linsp(Normal_AS01_L02[1:601,5],101)
front_AS01_L02 = linsp(Normal_AS01_L02[1:601,6],101)
all_AS01_L02 = linsp(Normal_AS01_L02[1:601,7],101)

# # Normal_AS01_L01(Python3)
Normal_AS01_L03 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_L03 = linsp(Normal_AS01_L03[1:601,4],101)
mid_AS01_L03 = linsp(Normal_AS01_L03[1:601,5],101)
front_AS01_L03 = linsp(Normal_AS01_L03[1:601,6],101)
all_AS01_L03 = linsp(Normal_AS01_L03[1:601,7],101)

# # Normal_AS01_R01(Python3)
Normal_AS01_R01 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_R01 = linsp(Normal_AS01_R01[1:601,4],101)
mid_AS01_R01 = linsp(Normal_AS01_R01[1:601,5],101)
front_AS01_R01 = linsp(Normal_AS01_R01[1:601,6],101)
all_AS01_R01 = linsp(Normal_AS01_R01[1:601,7],101)

# # Normal_AS01_R02(Python3)
Normal_AS01_R02 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_R02 = linsp(Normal_AS01_R02[1:601,4],101)
mid_AS01_R02 = linsp(Normal_AS01_R02[1:601,5],101)
front_AS01_R02 = linsp(Normal_AS01_R02[1:601,6],101)
all_AS01_R02 = linsp(Normal_AS01_R02[1:601,7],101)

# # Normal_AS01_R01(Python3)
Normal_AS01_R03 = np.genfromtxt('Data_test/Normal/S01/Standing/EVA/Non/Area_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS01_R03 = linsp(Normal_AS01_R03[1:601,4],101)
mid_AS01_R03 = linsp(Normal_AS01_R03[1:601,5],101)
front_AS01_R03 = linsp(Normal_AS01_R03[1:601,6],101)
all_AS01_R03 = linsp(Normal_AS01_R03[1:601,7],101)

# S02
# back_PS02_L01
# Normal_PS02_L01(Python3)
Normal_PS02_L01 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_L01 = linsp(Normal_PS02_L01[1:601,4],101)
mid_PS02_L01 = linsp(Normal_PS02_L01[1:601,5],101)
front_PS02_L01 = linsp(Normal_PS02_L01[1:601,6],101)
all_PS02_L01 = linsp(Normal_PS02_L01[1:601,7],101)

# # Normal_PS02_L02(Python3)
Normal_PS02_L02 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_L02 = linsp(Normal_PS02_L02[1:601,4],101)
mid_PS02_L02 = linsp(Normal_PS02_L02[1:601,5],101)
front_PS02_L02 = linsp(Normal_PS02_L02[1:601,6],101)
all_PS02_L02 = linsp(Normal_PS02_L02[1:601,7],101)

# # Normal_PS02_L01(Python3)
Normal_PS02_L03 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_L03 = linsp(Normal_PS02_L03[1:601,4],101)
mid_PS02_L03 = linsp(Normal_PS02_L03[1:601,5],101)
front_PS02_L03 = linsp(Normal_PS02_L03[1:601,6],101)
all_PS02_L03 = linsp(Normal_PS02_L03[1:601,7],101)

# # Normal_PS02_R01(Python3)
Normal_PS02_R01 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_R01 = linsp(Normal_PS02_R01[1:601,4],101)
mid_PS02_R01 = linsp(Normal_PS02_R01[1:601,5],101)
front_PS02_R01 = linsp(Normal_PS02_R01[1:601,6],101)
all_PS02_R01 = linsp(Normal_PS02_R01[1:601,7],101)

# # Normal_PS02_R02(Python3)
Normal_PS02_R02 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_R02 = linsp(Normal_PS02_R02[1:601,4],101)
mid_PS02_R02 = linsp(Normal_PS02_R02[1:601,5],101)
front_PS02_R02 = linsp(Normal_PS02_R02[1:601,6],101)
all_PS02_R02 = linsp(Normal_PS02_R02[1:601,7],101)

# # Normal_PS02_R01(Python3)
Normal_PS02_R03 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Pressure_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS02_R03 = linsp(Normal_PS02_R03[1:601,4],101)
mid_PS02_R03 = linsp(Normal_PS02_R03[1:601,5],101)
front_PS02_R03 = linsp(Normal_PS02_R03[1:601,6],101)
all_PS02_R03 = linsp(Normal_PS02_R03[1:601,7],101)

# back_AS02_L01
# Normal_AS02_L01(Python3)
Normal_AS02_L01 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_L01 = linsp(Normal_AS02_L01[1:601,4],101)
mid_AS02_L01 = linsp(Normal_AS02_L01[1:601,5],101)
front_AS02_L01 = linsp(Normal_AS02_L01[1:601,6],101)
all_AS02_L01 = linsp(Normal_AS02_L01[1:601,7],101)

# # Normal_AS02_L02(Python3)
Normal_AS02_L02 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_L02 = linsp(Normal_AS02_L02[1:601,4],101)
mid_AS02_L02 = linsp(Normal_AS02_L02[1:601,5],101)
front_AS02_L02 = linsp(Normal_AS02_L02[1:601,6],101)
all_AS02_L02 = linsp(Normal_AS02_L02[1:601,7],101)

# # Normal_AS02_L01(Python3)
Normal_AS02_L03 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_L03 = linsp(Normal_AS02_L03[1:601,4],101)
mid_AS02_L03 = linsp(Normal_AS02_L03[1:601,5],101)
front_AS02_L03 = linsp(Normal_AS02_L03[1:601,6],101)
all_AS02_L03 = linsp(Normal_AS02_L03[1:601,7],101)

# # Normal_AS02_R01(Python3)
Normal_AS02_R01 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_R01 = linsp(Normal_AS02_R01[1:601,4],101)
mid_AS02_R01 = linsp(Normal_AS02_R01[1:601,5],101)
front_AS02_R01 = linsp(Normal_AS02_R01[1:601,6],101)
all_AS02_R01 = linsp(Normal_AS02_R01[1:601,7],101)

# # Normal_AS02_R02(Python3)
Normal_AS02_R02 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_R02 = linsp(Normal_AS02_R02[1:601,4],101)
mid_AS02_R02 = linsp(Normal_AS02_R02[1:601,5],101)
front_AS02_R02 = linsp(Normal_AS02_R02[1:601,6],101)
all_AS02_R02 = linsp(Normal_AS02_R02[1:601,7],101)

# # Normal_AS02_R01(Python3)
Normal_AS02_R03 = np.genfromtxt('Data_test/Normal/S02/Standing/EVA/Non/Area_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS02_R03 = linsp(Normal_AS02_R03[1:601,4],101)
mid_AS02_R03 = linsp(Normal_AS02_R03[1:601,5],101)
front_AS02_R03 = linsp(Normal_AS02_R03[1:601,6],101)
all_AS02_R03 = linsp(Normal_AS02_R03[1:601,7],101)


# S03
# back_PS03_L01
# Normal_PS03_L01(Python3)
Normal_PS03_L01 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_L01 = linsp(Normal_PS03_L01[1:601,4],101)
mid_PS03_L01 = linsp(Normal_PS03_L01[1:601,5],101)
front_PS03_L01 = linsp(Normal_PS03_L01[1:601,6],101)
all_PS03_L01 = linsp(Normal_PS03_L01[1:601,7],101)

# # Normal_PS03_L02(Python3)
Normal_PS03_L02 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_L02 = linsp(Normal_PS03_L02[1:601,4],101)
mid_PS03_L02 = linsp(Normal_PS03_L02[1:601,5],101)
front_PS03_L02 = linsp(Normal_PS03_L02[1:601,6],101)
all_PS03_L02 = linsp(Normal_PS03_L02[1:601,7],101)

# # Normal_PS03_L01(Python3)
Normal_PS03_L03 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_L03 = linsp(Normal_PS03_L03[1:601,4],101)
mid_PS03_L03 = linsp(Normal_PS03_L03[1:601,5],101)
front_PS03_L03 = linsp(Normal_PS03_L03[1:601,6],101)
all_PS03_L03 = linsp(Normal_PS03_L03[1:601,7],101)

# # Normal_PS03_R01(Python3)
Normal_PS03_R01 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_R01 = linsp(Normal_PS03_R01[1:601,4],101)
mid_PS03_R01 = linsp(Normal_PS03_R01[1:601,5],101)
front_PS03_R01 = linsp(Normal_PS03_R01[1:601,6],101)
all_PS03_R01 = linsp(Normal_PS03_R01[1:601,7],101)

# # Normal_PS03_R02(Python3)
Normal_PS03_R02 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_R02 = linsp(Normal_PS03_R02[1:601,4],101)
mid_PS03_R02 = linsp(Normal_PS03_R02[1:601,5],101)
front_PS03_R02 = linsp(Normal_PS03_R02[1:601,6],101)
all_PS03_R02 = linsp(Normal_PS03_R02[1:601,7],101)

# # Normal_PS03_R01(Python3)
Normal_PS03_R03 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Pressure_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_PS03_R03 = linsp(Normal_PS03_R03[1:601,4],101)
mid_PS03_R03 = linsp(Normal_PS03_R03[1:601,5],101)
front_PS03_R03 = linsp(Normal_PS03_R03[1:601,6],101)
all_PS03_R03 = linsp(Normal_PS03_R03[1:601,7],101)

# back_AS03_L01
# Normal_AS03_L01(Python3)
Normal_AS03_L01 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_L01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_L01 = linsp(Normal_AS03_L01[1:601,4],101)
mid_AS03_L01 = linsp(Normal_AS03_L01[1:601,5],101)
front_AS03_L01 = linsp(Normal_AS03_L01[1:601,6],101)
all_AS03_L01 = linsp(Normal_AS03_L01[1:601,7],101)

# # Normal_AS03_L02(Python3)
Normal_AS03_L02 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_L02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_L02 = linsp(Normal_AS03_L02[1:601,4],101)
mid_AS03_L02 = linsp(Normal_AS03_L02[1:601,5],101)
front_AS03_L02 = linsp(Normal_AS03_L02[1:601,6],101)
all_AS03_L02 = linsp(Normal_AS03_L02[1:601,7],101)

# # Normal_AS03_L01(Python3)
Normal_AS03_L03 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_L03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_L03 = linsp(Normal_AS03_L03[1:601,4],101)
mid_AS03_L03 = linsp(Normal_AS03_L03[1:601,5],101)
front_AS03_L03 = linsp(Normal_AS03_L03[1:601,6],101)
all_AS03_L03 = linsp(Normal_AS03_L03[1:601,7],101)

# # Normal_AS03_R01(Python3)
Normal_AS03_R01 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_R01.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_R01 = linsp(Normal_AS03_R01[1:601,4],101)
mid_AS03_R01 = linsp(Normal_AS03_R01[1:601,5],101)
front_AS03_R01 = linsp(Normal_AS03_R01[1:601,6],101)
all_AS03_R01 = linsp(Normal_AS03_R01[1:601,7],101)

# # Normal_AS03_R02(Python3)
Normal_AS03_R02 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_R02.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_R02 = linsp(Normal_AS03_R02[1:601,4],101)
mid_AS03_R02 = linsp(Normal_AS03_R02[1:601,5],101)
front_AS03_R02 = linsp(Normal_AS03_R02[1:601,6],101)
all_AS03_R02 = linsp(Normal_AS03_R02[1:601,7],101)

# # Normal_AS03_R01(Python3)
Normal_AS03_R03 = np.genfromtxt('Data_test/Normal/S03/Standing/EVA/Non/Area_R03.csv', delimiter=',',skip_header=43, skip_footer=1)
back_AS03_R03 = linsp(Normal_AS03_R03[1:601,4],101)
mid_AS03_R03 = linsp(Normal_AS03_R03[1:601,5],101)
front_AS03_R03 = linsp(Normal_AS03_R03[1:601,6],101)
all_AS03_R03 = linsp(Normal_AS03_R03[1:601,7],101)

