import numpy as np
import re

import IDTP_sender as IDTProtocol

valDict = {"AP_Rear" : [],"AP_Mid" : [],"AP_Front" : [],"AP_Total" : [],
			"CA_Rear" : [],"CA_Mid" : [],"CA_Front" : [],"CA_Total" : [],
			"PP_Rear" : [],"PP_Mid" : [],"PP_Front" : [],"PP_Total" : [],
			"COP_Rear" : [],"COP_Mid" : [],"COP_Front" : [],"COP_Total" : [],"COP_Total_Total" : []}

patientArray = []

labels = ["AP_Rear", "AP_Mid", "AP_Front", "AP_Total", 
			"CA_Rear", "CA_Mid", "CA_Front", "CA_Total", 
			"PP_Rear", "PP_Mid", "PP_Front", "PP_Total", 
			"COP_Rear", "COP_Mid", "COP_Front", "COP_Total", "COP_Total_Total"] 
trial_num = 3
package = []
featureNumber = 4

def ReadLRData(trial,patient):
	Pressure_L = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/AP/Pressure_L0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	Pressure_R = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/AP/Pressure_R0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	Area_L     = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/CA/Area_L0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	Area_R     = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/CA/Area_R0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	PP_L       = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/PP/PP_L0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	PP_R       = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/PP/PP_R0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	COP_L      = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/COP/COP_L0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	COP_R      = np.genfromtxt('Data/Normal/S0'+str(patient)+'/Standing/EVA/Non/COP/COP_R0'+str(trial)+'.csv', delimiter=',',skip_header=43, skip_footer=1)
	Pressure = [Pressure_L,Pressure_R]
	Area     = [Area_L,Area_R]
	PP       = [PP_L,PP_R]
	COP      = [COP_L,COP_R]
	package  = [Pressure, Area, PP, COP]
	return package

def fetch_from_CSV(file):
	rear    = file[1:601,4]
	mid     = file[1:601,5]
	front   = file[1:601,6]
	total   = file[1:601,7]
	package = [rear,mid,front,total]
	return package

def fetch_from_CSV_COP(file):
	rear    = [file[1:601,6],file[1:601,7]]
	mid     = [file[1:601,8],file[1:601,9]]
	front   = [file[1:601,10],file[1:601,11]]
	total   = [file[1:601,12],file[1:601,13]]
	totalOT = [file[1:601,3],file[1:601,4]]
	package = [rear,mid,front,total,totalOT]
	return package

for patient in range(0,int(input("Number of Patient = "))):
	trialArray = []
	for trial in range(0,trial_num):
		flags = 0
		package = ReadLRData(int(trial) + 1,int(patient) + 1)
		for index in range(0,featureNumber):
			if index == 0:
				# Pressure
				leftData = fetch_from_CSV(package[index][0]) 
				rightData = fetch_from_CSV(package[index][1])
				for i in range(0,4):
					valDict[labels[flags]] = [leftData[i], rightData[i]]
					flags = flags + 1
			elif index == 1:
				# Area
				leftData = fetch_from_CSV(package[index][0]) 
				rightData = fetch_from_CSV(package[index][1])
				for i in range(0,4):
					valDict[labels[flags]] = [leftData[i], rightData[i]]
					flags = flags + 1
			elif index == 2:
				# PP
				leftData = fetch_from_CSV(package[index][0]) 
				rightData = fetch_from_CSV(package[index][1])
				for i in range(0,4):
					valDict[labels[flags]] = [leftData[i], rightData[i]]
					flags = flags + 1
			elif index == 3:
				# COP
				leftData = fetch_from_CSV_COP(package[index][0]) 
				rightData = fetch_from_CSV_COP(package[index][1])
				for i in range(0,5):
					valDict[labels[flags]] = [leftData[i], rightData[i]]
					flags = flags + 1
		trialArray.append(valDict)
	print(trialArray)
	patientArray.append(trialArray)

IDTProtocol.IDTPSender(patientArray)





			