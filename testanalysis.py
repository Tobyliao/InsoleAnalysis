import csv 
import numpy as np
import matplotlib.pyplot as plt

import testfunc as data
labels = [  "AP_Rear", "AP_Mid", "AP_Front", "AP_Total", 
			"CA_Rear", "CA_Mid", "CA_Front", "CA_Total", 
			"PP_Rear", "PP_Mid", "PP_Front", "PP_Total", 
			"COP_Rear", "COP_Mid", "COP_Front", "COP_Total", 
			"COP_Total_Total" ] 
class Buttons:
    def __init__(self): #initial the Data
        self.axcolor= 'lightgoldenrodyellow'
        self.rax = plt.axes([0.01, 0.3, 0.06, 0.301], axisbg=self.axcolor)
        self.radio = RadioButtons(self.rax, ( 'blue', 'red', 'green', 'all'), active=0)
        self.del_ax = plt.axes([0.01, 0.25, 0.085, 0.035])
        self.button_save = Button(self.del_ax, 'Save', color=self.axcolor, hovercolor='0.975')
        self.label = "all"
        self.radio.on_clicked(plot)
        self.button_save.on_clicked(save)

    def genBtn(self):
        plt.clf()
        plt.suptitle("Normal_S0%s_BStanding"%(number),x=0.5)
        self.__init__()
